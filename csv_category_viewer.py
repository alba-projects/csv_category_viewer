#!/usr/bin/env python3

import sys
import csv
from treelib import Node, Tree


def main():
    tree = Tree()

    # 1st argument is the filename
    filename = sys.argv[1]
    # Create the tree root
    tree.create_node('_', '/')
    is_bad_parent = False

    # Print nicely the filename
    nb_sep = 90 - len(filename)
    sep_s, sep_e = ((nb_sep//2), ((nb_sep//2)+(nb_sep%2)))
    print(("=" * sep_s + " %s " + "=" * sep_e) % filename)
    # Open the file
    file = open(filename)
    # Convert it as csv object
    csvreader = csv.reader(file)
    # Get first row (header)
    header = next(csvreader)
    # Get others row (data)
    rows = [row for row in csvreader]
    # Close the file
    file.close()

    # Get categories from column 35 of Woocommerce csv file
    category_list = [elt[35] for elt in rows]

    # Create assumed presents categories in wordpress
    tree.create_node("Collections", "Collections", parent='/')
    for run_nb in  [1, 2]:
        for idx, cat_str in enumerate(category_list):
            entries_checker = ["Collections"]
            for cat in cat_str.split(','):
                cat_path = cat.split('>')
                try:
                    if len(cat_path) == 1:
                        entries_checker += [cat_path[0]]
                        tree.create_node(cat_path[0], cat_path[0], parent='Collections')
                    else:
                        parent, child = cat_path[-2:]
                        pre_parent = cat_path[-3] if len(cat_path) > 2 else ''
                        if run_nb == 2 and parent not in entries_checker:
                            if not is_bad_parent:
                                is_bad_parent = True
                                tree.create_node('BAD_PARENT', 'BAD_PARENT', '/')
                            print("Error:", "line ", str(idx + 2) + ':', cat_str, "=> '" + parent + "'", "is undefined")
                            tree.create_node('[' + parent + "]", '[' + parent + "]", parent='BAD_PARENT')
                            tree.create_node('[' + child + "]", '[' + child + "]", parent='[' + parent + "]")
                        else:
                            entries_checker += [child]
                            tree.create_node(child, parent+child, parent=pre_parent+parent)
                except:
                    pass

    # Show the category tree
    tree.show(stdout=False)
    print(tree.show(stdout=False))

main()
